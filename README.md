# Uses Firebase Hosting
- 1 Page Simple Website

- Add a .firebaserc file with the following content to the project's root directory:
```
{
  "projects": {
    "default": "YOUR_FIREBASE_PROJECT_NAME"
  }
}

```

Make sure that you have the npm package `firebase-tools` added. Run `firebase serve` while testing, and `firebase deploy --only hosting` to deploy to Firebase.

Feel free to edit the text in the Index.html file and change the images stored in the `app/images` directory.
